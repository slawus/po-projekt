package poprojekt;

import java.util.ArrayList;
import poprojekt.core.Model;

import JaCoP.constraints.Among;
import JaCoP.constraints.Count;
import JaCoP.core.IntVar;
import JaCoP.core.IntervalDomain;

/**
 * A model to solve Akari problem.
 *
 * @author Slawomir Pruchnik
 */
public class AkariModel extends Model
{

    /**
     * Description of a problem.
     */
    public Description description;

    //variables
    protected ArrayList<ArrayList<IntVar>> verticalParts, horizontalParts;
    protected ArrayList<IntVar> neighboursOf[][];
    protected IntVar elements[][];

    /**
     * Describes problem details.
     */
    static public class Description
    {

        /**
         * board description
         */
        public char[][] board;

        /**
         * nr of rows in a board
         */
        public int nrOfRows;
        /**
         * nr of columns in a board
         */
        public int nrOfCols;

        public Description(Description problem)
        {
            this.nrOfRows = problem.nrOfRows;
            this.nrOfCols = problem.nrOfCols;
            this.board = new char[nrOfRows][nrOfCols];
            
            for(int i = 0; i < nrOfRows; i++)
            {
                for(int j = 0; j < nrOfCols; j++)
                {
                    this.board[i][j] = problem.board[i][j];
                }
            }
        }

        public Description()
        {
        }
    }

    /**
     * Preparing problem model - creates variables and constraints.
     *
     * Desribes problem as an Akari problem.
     */
    @Override
    public void prepare()
    {
        prepareFields();
        parseBoard();
        createConstraints();

        Utils.Debug.printCharMatrix(description.board, 7, 7);
    }

    /**
     * Initialize of private fields.
     */
    private void prepareFields()
    {
        verticalParts = new ArrayList();
        horizontalParts = new ArrayList();
        elements = new IntVar[description.nrOfRows][description.nrOfCols];
        neighboursOf = new ArrayList[description.nrOfRows][description.nrOfCols];

        for (int i = 0; i < description.nrOfRows; i++)
        {
            for (int j = 0; j < description.nrOfCols; j++)
            {
                neighboursOf[i][j] = new ArrayList();
            }
        }
    }

    /**
     * Parse board description.
     *
     * Parse problem details, dividing rooms into groups.
     */
    protected void parseBoard()
    {
        createElements();
        findHorizontalParts();
        findVerticalParts();
    }

    /**
     * Creates variables denoting fields in a board.
     */
    protected void createElements()
    {
        for (int i = 0; i < description.nrOfRows; i++)
        {
            ArrayList<IntVar> neighbours = new ArrayList();

            for (int j = 0; j < description.nrOfCols; j++)
            {
                if (description.board[i][j] == '-')
                {
                    //create net IntVar with domain  {0, 1} ( 1 - put bulb here)
                    elements[i][j] = new IntVar(store, "f" + i + j, 0, 1);

                    //add it to vars
                    vars.add(elements[i][j]);

                }
            }
        }
    }

    /**
     * Divide rooms into groups with same horizontal passage.
     */
    protected void findHorizontalParts()
    {
        for (int i = 0; i < description.nrOfRows; i++)
        {
            ArrayList<IntVar> neighbours = new ArrayList();

            for (int j = 0; j < description.nrOfCols; j++)
            {
                if (description.board[i][j] == '-')
                {
                    //remember fileds that are in same row, without wall between them
                    neighbours.add(elements[i][j]);
                }
                else
                {
                    if (!neighbours.isEmpty())
                    {
                        horizontalParts.add(neighbours);
                        //save neighbours for every field
                        int begin = j - neighbours.size();
                        for (int k = begin; k < j; k++)
                        {
                            neighboursOf[i][k].addAll(neighbours);
                        }
                    }

                    neighbours = new ArrayList();
                }
            }

            if (!neighbours.isEmpty())
            {
                horizontalParts.add(neighbours);
                //save neighbours for every field
                int begin = description.nrOfCols - neighbours.size();
                for (int k = begin; k < description.nrOfCols; k++)
                {
                    neighboursOf[i][k].addAll(neighbours);
                }
            }
        }
    }

    /**
     * Divide rooms into groups with same vertical passage.
     */
    protected void findVerticalParts()
    {
        for (int j = 0; j < description.nrOfCols; j++)
        {
            ArrayList<IntVar> neighbours = new ArrayList();

            for (int i = 0; i < description.nrOfRows; i++)
            {
                if (description.board[i][j] == '-')
                {
                    //remember fileds that are in same row, without wall between them
                    neighbours.add(elements[i][j]);
                }
                else
                {
                    if (!neighbours.isEmpty())
                    {
                        verticalParts.add(neighbours);
                        //save neighbours for every field
                        int begin = i - neighbours.size();
                        for (int k = begin; k < i; k++)
                        {
                            neighboursOf[k][j].addAll(neighbours);
                        }
                    }

                    neighbours = new ArrayList();
                }
            }

            if (!neighbours.isEmpty())
            {
                verticalParts.add(neighbours);
                //save neighbours for every field
                int begin = description.nrOfRows - neighbours.size();
                for (int k = begin; k < description.nrOfRows; k++)
                {
                    neighboursOf[k][j].addAll(neighbours);
                }
            }
        }
    }

    /**
     * Creates constraintes.
     *
     * Defining the Akari problem.
     */
    protected void createConstraints()
    {
        //limit to max 1 bulb in a row/column
        createBulbLimitConstraint();
        //make sure that there is always at least one bulb lightning each field
        createLightInEveryRoomConstraint();
        //limit nr of bulbs next to wall with factor
        createBulbNearWallContraint();
    }

    /**
     * Creates bulb limit constraint.
     *
     * Limits bulb nr to max 1 in every corridor
     */
    protected void createBulbLimitConstraint()
    {
        ArrayList<ArrayList<IntVar>> union = new ArrayList(horizontalParts);
        union.addAll(verticalParts);
        for (ArrayList<IntVar> part : union)
        {
            IntVar var = new IntVar(store, 0, 1);
            //vars.add(var);
            store.impose(new Among(part, new IntervalDomain(1, 1), var));
        }
    }

    /**
     * Create "light in every room" constraint.
     *
     * Makes sure that there is always at least one bulb lightning each field
     */
    protected void createLightInEveryRoomConstraint()
    {
        for (int i = 0; i < description.nrOfRows; i++)
        {
            for (int j = 0; j < description.nrOfCols; j++)
            {
                if (description.board[i][j] == '-')
                {
                    IntVar var = new IntVar(store, "CountAll" + i + j, 1, 1);
                    var.addDom(2, 2);
                    //vars.add(var);
                    store.impose(new Among(neighboursOf[i][j], new IntervalDomain(1, 2), var));
                }
            }
        }
    }

    /**
     * Create constraint, limitng nr of bulbs next to walls.
     *
     * Makes sure that there is always right number of bulbs next to the wall
     * which demands it.
     */
    protected void createBulbNearWallContraint()
    {
        for (int i = 0; i < description.nrOfRows; i++)
        {
            for (int j = 0; j < description.nrOfCols; j++)
            {
                if (description.board[i][j] >= '0' && description.board[i][j] <= '4')
                {
                    int nrOfBulbs = description.board[i][j] - '0';
                    //nrOfBulbs = 2;
                    IntVar var = new IntVar(store, "Wall" + i + j, nrOfBulbs, nrOfBulbs);
                    //vars.add(var);

                    //find all neighbour rooms
                    ArrayList<IntVar> neighbours = new ArrayList();
                    if (i - 1 >= 0 && elements[i - 1][j] != null)
                    {
                        neighbours.add(elements[i - 1][j]);
                    }

                    if (i + 1 < description.nrOfRows && elements[i + 1][j] != null)
                    {
                        neighbours.add(elements[i + 1][j]);
                    }

                    if (j - 1 >= 0 && elements[i][j - 1] != null)
                    {
                        neighbours.add(elements[i][j - 1]);
                    }

                    if (j + 1 < description.nrOfCols && elements[i][j + 1] != null)
                    {
                        neighbours.add(elements[i][j + 1]);
                    }

                    //create constraint
                    store.impose(new Count(neighbours, var, 1));
                }
            }
        }
    }

    protected char[][] finalResult()
    {
        for (int i = 0; i < description.nrOfRows; i++)
        {
            for (int j = 0; j < description.nrOfCols; j++)
            {
                if (elements[i][j] != null && elements[i][j].value() == 1)
                {
                    description.board[i][j] = '*';
                }
            }
        }

        return description.board;
    }

    /**
     * Returns result of solving as board description.
     *
     * @return Description problem description filled with bulbs.
     */
    public Description getResult()
    {
        Description d = new Description(description);

        for (int i = 0; i < description.nrOfRows; i++)
        {
            for (int j = 0; j < description.nrOfCols; j++)
            {
                if (elements[i][j] != null && elements[i][j].value() == 1)
                {
                    d.board[i][j] = '*';
                }
            }
        }

        return d;
    }

    private void fakeBoardDescription()
    {
//        boardDescription = new char[][]
//        {
//            {
//                '-', '-', '-', '3', '-', '-', '-'
//            },
//            {
//                '-', '0', '-', '-', '-', '0', '-'
//            },
//            {
//                '-', '-', '-', 'x', '-', '-', '-'
//            },
//            {
//                '2', '-', '1', 'x', '2', '-', 'x'
//            },
//            {
//                '-', '-', '-', 'x', '-', '-', '-'
//            },
//            {
//                '-', 'x', '-', '-', '-', '2', '-'
//            },
//            {
//                '-', '-', '-', '2', '-', '-', '-'
//            }
//        };
//        boardDescription = new char[][]
//        {
//            {
//                '-', '-', '-', 'x', '-', '-', '-'
//            },
//            {
//                '-', '-', 'x', '-', 'x', '-', '-'
//            },
//            {
//                '-', '3', '-', '-', '-', '3', '-'
//            },
//            {
//                '2', '-', '-', 'x', '-', '-', '2'
//            },
//            {
//                '-', '2', '-', '-', '-', '3', '-'
//            },
//            {
//                '-', '-', 'x', '-', '2', '-', '-'
//            },
//            {
//                '-', '-', '-', 'x', '-', '-', '-'
//            }
//        };

        char[][] boardDescription = new char[][]
        {
            {
                '-', '-', '-', '-', '-', '-', '-'
            },
            {
                '-', '2', '-', '0', '-', '1', '-'
            },
            {
                '-', '-', '-', '-', '-', '-', '-'
            },
            {
                '-', '0', '-', '-', '-', '2', '-'
            },
            {
                '-', '-', '-', '-', '-', '-', '-'
            },
            {
                '-', '0', '-', '1', '-', '2', '-'
            },
            {
                '-', '-', '-', '-', '-', '-', '-'
            }
        };

        Description d = new Description();
        d.board = boardDescription;
        d.nrOfCols = 7;
        d.nrOfRows = 7;

        this.description = d;
    }
}
