package poprojekt;

import poprojekt.core.Model;

import JaCoP.constraints.Circuit;
import JaCoP.constraints.Element;
import JaCoP.constraints.IfThen;
import JaCoP.constraints.IfThenElse;
import JaCoP.constraints.Sum;
import JaCoP.constraints.XeqC;
import JaCoP.constraints.XlteqY;

import JaCoP.core.IntVar;
import java.util.ArrayList;

/**
 *
 * It models Travelling Salesman Problem with Time Windows (TSPTW).
 *
 * @author Slawomir Pruchnik
 *
 */
public class TSPTWModel extends Model
{

    /**
     * Description of a problem.
     */
    public Description description;

    //variables
    IntVar[] cities;
    IntVar[] costs;
    IntVar[] limit;
    IntVar[] limitCosts;
    IntVar[] citiesOrder;
    IntVar[] subdistances;
    
    /**
     * Desctibes problem details.
     */
    public static class Description
    {

        /**
         * No of cities.
         */
        public int noCities;
        /**
         * Distances between cities.
         */
        public int[][] distance;
        /**
         * Distance limits, in which salesman should reach each city.
         */
        public int[] limit;
        
        /**
         * Cost of non-fulfilment of limit conditions.
         * 
         * Set to null if limit condidions are hard.
         */
        public int[] limitCost;
        
        /**
         * City from which salesman starts.
         */
        public int startCity;
    }

    protected void fakeProblemDescription()
    {
        int[][] distance =
        {
            {
                1000, 85, 110, 94, 71, 76, 25, 56, 94, 67
            },
            {
                85, 1000, 26, 70, 62, 60, 63, 62, 70, 49
            },
            {
                110, 26, 1000, 71, 87, 89, 88, 87, 93, 73
            },
            {
                94, 70, 71, 1000, 121, 19, 82, 106, 124, 105
            },
            {
                71, 62, 87, 121, 1000, 104, 53, 24, 8, 13
            },
            {
                76, 60, 89, 19, 104, 1000, 65, 89, 108, 93
            },
            {
                25, 63, 88, 82, 53, 65, 1000, 30, 57, 46
            },
            {
                56, 62, 87, 106, 24, 89, 30, 1000, 23, 20
            },
            {
                94, 70, 93, 124, 8, 108, 57, 23, 1000, 20
            },
            {
                67, 49, 73, 105, 13, 93, 46, 20, 20, 1000
            }
        };

        int[] aLimit =
        {
            400, 400, 400, 400, 72, 400, 400, 400, 400, 400
        };
        
        int[] limitCosts =
        {
            1000, 10, 10, 10, 10, 10, 10, 10, 10, 10
        };

        Description aDescription = new Description();
        aDescription.noCities = 10;
        aDescription.distance = distance;
        aDescription.limit = aLimit;
        aDescription.startCity = 1;
        aDescription.limitCost = limitCosts;

        this.description = aDescription;
    }

    /**
     * Preparing problem model - creates variables and constraints.
     */
    @Override
    public void prepare()
    {
        //just for tests
        //fakeProblemDescription();
        createVariables();
        createHardConstraints();
        createLimitConstraint();
        defineCostFunction();
    }

    protected void createVariables()
    {
        // Denotes a city to go to from index city
        cities = new IntVar[description.noCities];
        // Denotes a cost of traveling between index city and next city
        costs = new IntVar[description.noCities];
        // Denotes distance limit in which salesman should visit index city 
        limit = new IntVar[description.noCities];
        // cities in order in which are visited
        citiesOrder = new IntVar[description.noCities];
        // distance after visiting indexNumber cities
        subdistances = new IntVar[description.noCities];
        // cost of unfillement of limti constraint
        limitCosts = new IntVar[description.noCities];

        for (int i = 0; i < cities.length; i++)
        {
            cities[i] = new IntVar(store, "cities[" + (i + 1) + "]", 1, cities.length);
            costs[i] = new IntVar(store, "costs[" + (i + 1) + "]", 0, 1000);
            limit[i] = new IntVar(store, "limits[" + (i + 1) + "]", 0, 1000);
            citiesOrder[i] = new IntVar(store, "inOrder[" + (i + 1) + "]", 1, description.noCities);
            limitCosts[i] = new IntVar(store, "limitCost["+(i+1)+"]", 0, 1000);
            
//            if(description.limitCost != null)
//            {
//                limitCosts[i].addDom(description.limitCost[i], description.limitCost[i]);
//            }
            

            vars.add(cities[i]);
            //vars.add(costs[i]);
            //vars.add(limit[i]);
            //vars.add(inOrder[i]);
        }

    }

    protected void createHardConstraints()
    {
        // Impose cuircuit constraint which makes sure that array cities is a hamiltonian circuit
        store.impose(new Circuit(cities));

        // Computes a cost of traveling between ith city and city[i]-th city
        for (int i = 0; i < cities.length; i++)
        {
            store.impose(new Element(cities[i], description.distance[i], costs[i]));
        }
    }

    protected void createLimitConstraint()
    {
        ArrayList<IntVar> distance = new ArrayList();

        //previously visited city - bgin from startCity
        IntVar prev = new IntVar(store, description.startCity, description.startCity);

        for (int i = 0; i < description.noCities; i++)
        {
            //select next city in order
            store.impose(new Element(prev, cities, citiesOrder[i]));
            //get it's limit value
            store.impose(new Element(citiesOrder[i], description.limit, limit[i]));

            //get cost of taken route
            subdistances[i] = new IntVar(store, "costsInOrder[" + (i + 1) + "]", 0, 1000);
            store.impose(new Element(prev, costs, subdistances[i]));

            //sum up distance already taken
            distance.add(subdistances[i]);
            IntVar subdistance = new IntVar(store, "subdistance[" + (i + 1) + "]", 0, 1000);
            store.impose(new Sum(distance, subdistance));
            
            if(description.limitCost == null)
            {
                store.impose(new XlteqY(subdistance, limit[i]));
            }
            else
            {
                store.impose(new IfThenElse(new XlteqY(subdistance, limit[i]), new XeqC(limitCosts[i], 0), new XeqC(limitCosts[i], description.limitCost[i])));
            }

            prev = citiesOrder[i];
        }
    }

    protected void defineCostFunction()
    {
        cost = new IntVar(store, "Cost", 0, 100000);

        // Computes overall cost of traveling
        // simply sum of all costs
        
        //if there are soft condition add their cost to base costs
        if(description.limitCost != null)
        {
            IntVar[] result = new IntVar[costs.length + limitCosts.length];
            System.arraycopy(costs, 0, result, 0,  costs.length);
            System.arraycopy(limitCosts, 0, result, costs.length, limitCosts.length);
            
            store.impose(new Sum(result, cost));
        }
        else
            store.impose(new Sum(costs, cost));
    }
    
    public int[] getResult()
    {
        int[] res = new int[description.noCities];
        
        for(int i = 0; i < description.noCities; i++)
        {
            res[i] = citiesOrder[i].value();
        }
        
        return res;
    }
}
