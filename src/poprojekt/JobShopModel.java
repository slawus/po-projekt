package poprojekt;

import JaCoP.constraints.XplusYlteqZ;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import poprojekt.core.Model;

import JaCoP.core.IntVar;
import JaCoP.constraints.Cumulative;
import JaCoP.constraints.Max;
import JaCoP.constraints.XplusYeqZ;

/**
 * Implementation of JobShop problem
 *
 * @author Slawomir Pruchnik
 */
public class JobShopModel extends Model
{

    /**
     * Description of a problem.
     */
    public Description description;

    //variables
    IntVar taskDuration[][];
    IntVar taskStartTime[][];

    /**
     * Describes problem details.
     */
    public static class Description
    {

        /**
         * Tasks to be done.
         */
        public ArrayList<Task> tasks;

        /**
         * Machines names.
         */
        public ArrayList<String> machines;
    }

    /**
     * Task Containter.
     *
     * Store information about a task.
     */
    public static class Task
    {

        /**
         * Order o machines.
         */
        public ArrayList<Integer> order;
        /**
         * Duraions of subtasks on each machine.
         */
        public ArrayList<Integer> durtaions;
        /**
         * Name of this task.
         */
        public String name;
        /**
         * Delay of starting this task.
         */
        public int startTime;

        /**
         * Constructor - create task with full description.
         *
         * @param name Name of a task
         * @param durationOnMachines Duraions of subtasks oneach machine.
         * @param orderOnMachines Order o machines used in succeeding subtasks.
         * @param startTime Start delay time.
         */
        public Task(String name, ArrayList<Integer> durationOnMachines, ArrayList<Integer> orderOnMachines, int startTime)
        {
            this.name = name;
            this.order = orderOnMachines;
            this.durtaions = durationOnMachines;
            this.startTime = startTime;
        }

        /**
         * Constructor - create task with name.
         *
         * @param name Name of a task
         */
        public Task(String name)
        {
            this.name = name;
        }
    }

    /**
     * Preparing problem model - creates variables and constraints.
     */
    @Override
    public void prepare()
    {
        //just for tests
        //fakeProblemDescription();

        checkDescriptionConsistency();

        createVariables();
        createCorrectSubtasksOrderConstraint();
        createOneTaskAtATimeConstraint();
        defineCostsVariable();
    }

    /**
     * Creates variables for problem, based on problem parameters.
     */
    protected void createVariables()
    {
        int nrOfMachines = description.machines.size();
        int nrOfTasks = description.tasks.size();

        //add vars for task parts
        taskDuration = new IntVar[nrOfTasks][nrOfMachines];
        taskStartTime = new IntVar[nrOfTasks][nrOfMachines];

        for (int taskId = 0; taskId < nrOfTasks; taskId++)
        {
            Task task = description.tasks.get(taskId);

            //vars describing duration of a subtask
            for (int machineId = 0; machineId < nrOfMachines; machineId++)
            {
                taskDuration[taskId][machineId] = new IntVar(store, task.name + description.machines.get(machineId) + "Duration", task.durtaions.get(machineId), task.durtaions.get(machineId));
            }

            //vars describing start time of a subtask
            for (int machineId : task.order)
            {
                taskStartTime[taskId][machineId] = new IntVar(store, task.name + description.machines.get(machineId) + "StartTime", task.startTime, 100000);
                vars.add(taskStartTime[taskId][machineId]);
            }
        }
    }

    /**
     * Creates costraint to assure that subtasks of each task will be executed
     * in correct order.
     */
    protected void createCorrectSubtasksOrderConstraint()
    {
        for (int taskId = 0; taskId < description.tasks.size(); taskId++)
        {
            Task task = description.tasks.get(taskId);

            int lastMachineId = -1;
            for (int machineId : task.order)
            {
                //add order of subtasks constraint
                if (lastMachineId > -1)
                {
                    //start time of a subtask must be equal or greater than endtime of previous subtask
                    store.impose(new XplusYlteqZ(taskStartTime[taskId][lastMachineId], taskDuration[taskId][lastMachineId], taskStartTime[taskId][machineId]));
                }

                lastMachineId = machineId;
            }
        }
    }

    /**
     * Creates constraint to aassure that there is max one task on machine at a
     * time.
     */
    protected void createOneTaskAtATimeConstraint()
    {
        //limit nr of tasks on each machine
        IntVar singleResource = new IntVar(store, 1, 1);

        for (int machineId = 0; machineId < description.machines.size(); machineId++)
        {
            ArrayList<IntVar> starts = new ArrayList();
            ArrayList<IntVar> durations = new ArrayList();
            ArrayList<IntVar> resources = new ArrayList();

            for (int taskId = 0; taskId < description.tasks.size(); taskId++)
            {
                //if task uses this machine, add it to list
                if (taskStartTime[taskId][machineId] != null)
                {
                    starts.add(taskStartTime[taskId][machineId]);
                    durations.add(taskDuration[taskId][machineId]);
                    resources.add(singleResource);
                }
            }

            //create constraint
            store.impose(new Cumulative(starts, durations, resources, singleResource));
        }
    }

    /**
     * Defines cost function.
     *
     * Cost of a solution is defined as time in which all tasks became
     * completed.
     */
    protected void defineCostsVariable()
    {
        //list of tasks end times 
        ArrayList<IntVar> taskEndTime = new ArrayList();

        for (int taskId = 0; taskId < description.tasks.size(); taskId++)
        {
            Task task = description.tasks.get(taskId);

            //last used machine
            int lastMachine = task.order.get(task.order.size() - 1);

            IntVar var = new IntVar(store, task.name + "EndTime", 0, 100000);
            vars.add(var);

            //end time of a task is defined as sum of last subtask start time and its duration 
            store.impose(new XplusYeqZ(taskStartTime[taskId][lastMachine], taskDuration[taskId][lastMachine], var));

            taskEndTime.add(var);
        }

        cost = new IntVar(store, "endTime", 0, 100000);
        vars.add(cost);

        //cost is max of all end times
        store.impose(new Max(taskEndTime, (IntVar) cost));
    }

    /**
     * Checks if provided problem description is correct.
     *
     * Problem is correct if tasks uses only machines with ids in range <0; nr
     * of machines - 1>
     */
    protected void checkDescriptionConsistency()
    {
        int nrOfMachines = description.machines.size();

        for (Task t : description.tasks)
        {
            if (Collections.max(t.order) > nrOfMachines || Collections.min(t.order) < 0)
            {
                throw new Error("Task need undefined machine");
            }
        }
    }

    final void fakeProblemDescription()
    {
        Description d = new Description();
        d.machines = new ArrayList(Arrays.asList("Cogito", "Focus", "Politics", "Newsweek"));
        d.tasks = new ArrayList();
        d.tasks.add(new Task("Alice", new ArrayList(Arrays.asList(30, 60, 2, 5)), new ArrayList(Arrays.asList(1, 0, 2, 3)), 0));
        d.tasks.add(new Task("Betty", new ArrayList(Arrays.asList(75, 25, 3, 10)), new ArrayList(Arrays.asList(0, 2, 1, 3)), 15));
        d.tasks.add(new Task("Charles", new ArrayList(Arrays.asList(15, 10, 5, 30)), new ArrayList(Arrays.asList(2, 0, 1, 3)), 15));
        d.tasks.add(new Task("Derek", new ArrayList(Arrays.asList(1, 1, 1, 90)), new ArrayList(Arrays.asList(3, 1, 0, 2)), 60));

        this.description = d;
    }
    
    /**
     * Returns array with variables containing solution (subtasks start times).
     * 
     * @return IntVar[][] startTimes.
     */
    public IntVar [][] getResult()
    {
       return taskStartTime;
    }
}
