package poprojekt.core;

import JaCoP.core.IntVar;
import JaCoP.core.Store;
import JaCoP.core.Var;

import JaCoP.search.ComparatorVariable;
import JaCoP.search.DepthFirstSearch;
import JaCoP.search.Indomain;
import JaCoP.search.SelectChoicePoint;
import JaCoP.search.SimpleSelect;

import java.util.ArrayList;

/**
 * Solver class.
 * 
 * Solves constraint problems.
 *
 * @author Slawomir Pruchnik
 */
public class Solver
{
    /**
     * ComparatorVariable object.
     * 
     * Desribes which variables should be choosen first.
     */
    public ComparatorVariable comparator;
    
    /**
     * Indomain object.
     * 
     * Describes the way in which values of variables should be choosen.
     */
    public Indomain inDomain;

    /**
     * Parametric constructor.
     * 
     * Allow to set comparator and indomain object on Solver initialization.
     * 
     * @param comparator ComparatorVariable object. Desribes which variables should be choosen first.
     * @param inDomain Indomain object. Describes the way in which values of variables should be choosen.
     */
    public Solver(ComparatorVariable comparator, Indomain inDomain)
    {
        this.comparator = comparator;
        this.inDomain = inDomain;
    }
    
    /**
     * Searching for solution, without optimal criterion.
     * 
     * 
     * @param store store from the model.
     * @param vars variables from the model.
     * @return true if solution found, in other case false
     */
    public boolean solve(Store store, ArrayList<Var> vars)
    {
        return solve(store, vars, null);
    }

    /**
     * Searching for optimal solution.
     * 
     * @param store store from the model.
     * @param vars variables from the model.
     * @param cost integer variable describing cos of solution.
     * @return true if solution found, in other case false
     */
    public boolean solve(Store store, ArrayList<Var> vars, IntVar cost)
    {
        long T1, T2;
        T1 = System.currentTimeMillis();

        SelectChoicePoint select = new SimpleSelect(vars.toArray(new Var[1]), comparator,
                inDomain);
        DepthFirstSearch search = new DepthFirstSearch();
        boolean result = search.labeling(store, select, cost);

        if (result)
        {
            //store.print();
        }

        T2 = System.currentTimeMillis();
        System.out.println("\n\t*** Execution time = " + (T2 - T1) + " ms");

        return result;
    }
}
