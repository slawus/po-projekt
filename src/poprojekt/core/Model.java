package poprojekt.core;

import JaCoP.core.IntVar;
import java.util.ArrayList;

import JaCoP.core.Store;
import JaCoP.core.Var;

/**
 * Generic model class.
 *
 * To implement any problem simply extend this class.
 *
 * @author Slawomir Pruchnik
 */
public abstract class Model
{

    /**
     * Store.
     */
    protected Store store;
    /**
     * Variables.
     */
    protected ArrayList<Var> vars;
    /**
     * Cost of solution.
     */
    protected IntVar cost;

    /**
     * Constructor. Constructs new model.
     */
    public Model()
    {
        store = new Store();
        vars = new ArrayList();
    }

    /**
     * Preparing problem model - creates variables and constraints. Must be
     * implemented by a concretized problem.
     */
    public abstract void prepare();

    /**
     * Getter of the store.
     *
     * @return Store object that contains all defined constraints.
     */
    public Store getStore()
    {
        return store;
    }

    /**
     * Getter of list of variables.
     *
     * @return ArrayList containing all variables defined in a problem.
     */
    public ArrayList<Var> getVariablesList()
    {
        return vars;
    }

    /**
     * Getter for the cost variable.
     *
     * @return IntVar object object defining cost of a solution.
     */
    public IntVar getCost()
    {
        return cost;
    }
}
