package Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author slawus
 */
public class ResizableTableModel extends AbstractTableModel
{

    protected int rows;
    protected int cols;
    protected Object[][] matrix;
    protected boolean[][] disabled;
    public String[] columnNames;

    public ResizableTableModel(int rows, int cols)
    {
        this.rows = rows;
        this.cols = cols;
        matrix = new Object[rows][cols];
        disabled = new boolean[rows][cols];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return String.class;
    }

    public Class<?> getRowClass(int rowIndex)
    {
        return String.class;
    }

    public void deleteColumn(int col)
    {
        Object[][] m = new Object[rows][cols + 1];

        int i = 0;
        for (Object[] row : matrix)
        {
            Object[] newRow = new Object[cols - 1];

            for (int j = 0; j < row.length; j++)
            {
                if (j < col)
                {
                    newRow[j] = row[j];
                }
                else if (j > col)
                {
                    newRow[j - 1] = row[j];
                }
            }

            m[i] = newRow;
            i++;
        }
        matrix = m;
        cols--;
        this.fireTableStructureChanged();
    }

    public void addColumn(int col)
    {
        Object[][] m = new Object[rows][cols + 1];
        boolean[][] d = new boolean[rows][cols + 1];

        int i = 0;
        for (Object[] row : matrix)
        {
            boolean[] disabledRow = disabled[i];

            Object[] newRow = new Object[cols + 1];
            boolean[] newDisabledRow = new boolean[cols + 1];

            // TODO: copy remaining values
            for (int j = 0; j < row.length; j++)
            {
                if (j < col)
                {
                    newRow[j] = row[j];
                    newDisabledRow[j] = disabledRow[j];
                }
                else
                {
                    newRow[j + 1] = row[j];
                    newDisabledRow[j + 1] = disabledRow[j];
                }
            }

            m[i] = newRow;
            d[i] = newDisabledRow;

            i++;
        }
        matrix = m;
        disabled = d;
        cols++;

        this.fireTableStructureChanged();
    }

    public void deleteRow(int rowNr)
    {
        Object[][] m = new Object[rows - 1][cols];
        boolean[][] d = new boolean[rows - 1][cols];

        int i = 0;
        for (Object[] row : matrix)
        {
            boolean[] disabledRow = disabled[i];

            if (i < rowNr)
            {
                m[i] = row;
                d[i] = disabledRow;
            }
            else if (i > rowNr)
            {
                m[i - 1] = row;
                d[i - 1] = disabledRow;
            }
            i++;
        }

        matrix = m;
        disabled = d;
        rows--;
        this.fireTableStructureChanged();
    }

    public void addRow(int rowNr)
    {
        Object[][] m = new Object[rows + 1][cols];
        boolean[][] d = new boolean[rows + 1][cols];
        
        m[rowNr] = new Object[cols];
        d[rowNr] = new boolean[cols];
        
        int i = 0;
        for (Object[] row : matrix)
        {
            boolean[] disabledRow = disabled[i];
            
            if (i < rowNr)
            {
                m[i] = row;
                d[i] = disabledRow;
            }
            else if (i >= rowNr)
            {
                m[i + 1] = row;
                d[i + 1] = disabledRow;
            }

            i++;
        }

        matrix = m;
        disabled = d;
        
        rows++;
        this.fireTableStructureChanged();
    }

    public void setColumnNumber(int nr)
    {
        if (nr < 0)
        {
            throw new Error("Nr of columns must be positive or 0");
        }

        int diff = this.cols - nr;

        //remove redundant columns
        for (int i = 0; i < diff; i++)
        {
            this.deleteColumn(this.cols-1);
        }

        //add missing columns
        for (int i = 0; i < -diff; i++)
        {
            this.addColumn(this.cols);
        }
    }

    public void setRowsNumber(int nr)
    {
        if (nr < 0)
        {
            throw new Error("Nr of columns must be positive or 0");
        }

        int diff = this.rows - nr;

        //remove redundant columns
        for (int i = 0; i < diff; i++)
        {
            this.deleteRow(this.rows-1);
        }

        //add missing columns
        for (int i = 0; i < -diff; i++)
        {
            this.addRow(this.rows);
        }
    }

    @Override
    public Object getValueAt(int i, int i1)
    {
        return matrix[i][i1];
    }

    @Override
    public void setValueAt(Object aValue, int row, int col)
    {
        matrix[row][col] = aValue;

        fireTableCellUpdated(row, col);
    }

    @Override
    public int getRowCount()
    {
        return rows;
    }

    @Override
    public int getColumnCount()
    {
        return cols;
    }

    @Override
    public boolean isCellEditable(int row, int col)
    {
        return !disabled[row][col];
    }

    @Override
    public String getColumnName(int i)
    {
        if(columnNames != null && columnNames[i] != null)
            return columnNames[i];
            
        return super.getColumnName(i); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public void disableCell(int row, int col)
    {
        disabled[row][col] = true;
        fireTableCellUpdated(row, col);
    }
}
