package GUI;

import JaCoP.core.IntVar;
import JaCoP.search.*;
import JaCoP.set.search.MaxCardDiff;
import JaCoP.set.search.MaxGlbCard;
import JaCoP.set.search.MaxLubCard;
import JaCoP.set.search.MinCardDiff;
import JaCoP.set.search.MinGlbCard;
import JaCoP.set.search.MinLubCard;
import Utils.MessageConsole;

import javax.swing.event.TableModelListener;

import poprojekt.AkariModel;
import Utils.ResizableTableModel;
import Utils.StringUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.HeadlessException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.gantt.XYTaskDataset;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.xy.IntervalXYDataset;
import poprojekt.JobShopModel;
import poprojekt.TSPTWModel;
import poprojekt.core.Solver;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author slawus
 */
public class MainFrame extends javax.swing.JFrame
{

    private ArrayList<AkariModel.Description> akariProblems;

    private AkariModel.Description akariDescription;

    private Object[] comparators;
    private Object[] inDomains;
    
    private MessageConsole messageConsoleAkari, messageConsoleJobShop, messageConsoleTSP;

    public MainFrame(ArrayList<AkariModel.Description> akariProblems, AkariModel.Description akariDescription, Object[] comparators, Object[] inDomains, MessageConsole console, JPanel AkariPanel, JPanel JobShopPanel, JPanel TSPPanel, JTabbedPane TabbedPane, JTextArea akariConsoleTextArea, JTable boardTable, JScrollPane boardTableScrollPane, JFormattedTextField colsTextField, JComboBox comparatorBox, JComboBox examplesBox, JComboBox inDomainBox, JLabel jLabel1, JLabel jLabel10, JLabel jLabel2, JLabel jLabel3, JLabel jLabel4, JLabel jLabel5, JLabel jLabel6, JLabel jLabel7, JLabel jLabel9, JPanel jPanel4, JPanel jPanel5, JScrollPane jScrollPane10, JScrollPane jScrollPane11, JScrollPane jScrollPane2, JScrollPane jScrollPane3, JScrollPane jScrollPane4, JScrollPane jScrollPane5, JScrollPane jScrollPane6, JScrollPane jScrollPane7, JScrollPane jScrollPane8, JScrollPane jScrollPane9, JTextPane jTextPane3, JTextPane jTextPane4, JTable jsOrderTable, JTextArea jsTextArea, JTable jsTimeTable, JFormattedTextField machineNrTextField, JFormattedTextField rowsTextField, JButton solveButton, JTextField starttownTextField, JFormattedTextField taskNrTextField, JFormattedTextField townNrTextField, JTable tspLimitCostTable, JTable tspLimitTable, JCheckBox tspSoftConditionsBox, JTextArea tspTexyArea, JTable tspTimesTable) throws HeadlessException
    {
        this.akariProblems = akariProblems;
        this.akariDescription = akariDescription;
        this.comparators = comparators;
        this.inDomains = inDomains;
        //this.console = console;
        this.AkariPanel = AkariPanel;
        this.JobShopPanel = JobShopPanel;
        this.TSPPanel = TSPPanel;
        this.TabbedPane = TabbedPane;
        this.akariConsoleTextArea = akariConsoleTextArea;
        this.boardTable = boardTable;
        this.boardTableScrollPane = boardTableScrollPane;
        this.colsTextField = colsTextField;
        this.comparatorBox = comparatorBox;
        this.examplesBox = examplesBox;
        this.inDomainBox = inDomainBox;
        this.jLabel1 = jLabel1;
        this.jLabel10 = jLabel10;
        this.jLabel2 = jLabel2;
        this.jLabel3 = jLabel3;
        this.jLabel4 = jLabel4;
        this.jLabel5 = jLabel5;
        this.jLabel6 = jLabel6;
        this.jLabel7 = jLabel7;
        this.jLabel9 = jLabel9;
        this.jPanel4 = jPanel4;
        this.jPanel5 = jPanel5;
        this.jScrollPane10 = jScrollPane10;
        this.jScrollPane11 = jScrollPane11;
        this.jScrollPane2 = jScrollPane2;
        this.jScrollPane3 = jScrollPane3;
        this.jScrollPane4 = jScrollPane4;
        this.jScrollPane5 = jScrollPane5;
        this.jScrollPane6 = jScrollPane6;
        this.jScrollPane7 = jScrollPane7;
        this.jScrollPane8 = jScrollPane8;
        this.jScrollPane9 = jScrollPane9;
        this.jTextPane3 = jTextPane3;
        this.jTextPane4 = jTextPane4;
        this.jsOrderTable = jsOrderTable;
        this.jsConsoleTextArea = jsTextArea;
        this.jsTimeTable = jsTimeTable;
        this.machineNrTextField = machineNrTextField;
        this.rowsTextField = rowsTextField;
        this.solveButton = solveButton;
        this.starttownTextField = starttownTextField;
        this.taskNrTextField = taskNrTextField;
        this.townNrTextField = townNrTextField;
        this.tspLimitCostTable = tspLimitCostTable;
        this.tspLimitTable = tspLimitTable;
        this.tspSoftConditionsBox = tspSoftConditionsBox;
        this.tspTexyArea = tspTexyArea;
        this.tspTimesTable = tspTimesTable;
    }

    /**
     * Creates new form JFrame
     */
    public MainFrame()
    {
        defineAkariProblems();
        akariDescription = akariProblems.get(0);

        initComponents();
        prepareView();
        
        messageConsoleAkari = new MessageConsole(akariConsoleTextArea);
        messageConsoleAkari.setMessageLines(200);
        messageConsoleJobShop = new MessageConsole(jsConsoleTextArea);
        messageConsoleJobShop.setMessageLines(200);
        messageConsoleTSP = new MessageConsole(tspTexyArea);
        messageConsoleTSP.setMessageLines(200);
        
        TabbedPaneStateChanged(null);
    }

    private void defineAkariProblems()
    {
        akariProblems = new ArrayList();
        AkariModel.Description problem = new AkariModel.Description();

        //1st
        problem.nrOfRows = 7;
        problem.nrOfCols = 7;
        problem.board = new char[][]
        {
            {
                '-', '-', '-', '-', '-', '-', '-'
            },
            {
                '-', '2', '-', '0', '-', '1', '-'
            },
            {
                '-', '-', '-', '-', '-', '-', '-'
            },
            {
                '-', '0', '-', '-', '-', '2', '-'
            },
            {
                '-', '-', '-', '-', '-', '-', '-'
            },
            {
                '-', '0', '-', '1', '-', '2', '-'
            },
            {
                '-', '-', '-', '-', '-', '-', '-'
            }
        };
        akariProblems.add(problem);

        //2nd
        problem = new AkariModel.Description();
        problem.nrOfRows = 7;
        problem.nrOfCols = 7;
        problem.board = new char[][]
        {
            {
                '-', '-', '-', '3', '-', '-', '-'
            },
            {
                '-', '0', '-', '-', '-', '0', '-'
            },
            {
                '-', '-', '-', 'x', '-', '-', '-'
            },
            {
                '2', '-', '1', 'x', '2', '-', 'x'
            },
            {
                '-', '-', '-', 'x', '-', '-', '-'
            },
            {
                '-', 'x', '-', '-', '-', '2', '-'
            },
            {
                '-', '-', '-', '2', '-', '-', '-'
            }
        };
        akariProblems.add(problem);

        //3rd
        problem = new AkariModel.Description();
        problem.nrOfRows = 7;
        problem.nrOfCols = 7;
        problem.board = new char[][]
        {
            {
                '-', '-', '-', 'x', '-', '-', '-'
            },
            {
                '-', '-', 'x', '-', 'x', '-', '-'
            },
            {
                '-', '3', '-', '-', '-', '3', '-'
            },
            {
                '2', '-', '-', 'x', '-', '-', '2'
            },
            {
                '-', '2', '-', '-', '-', '3', '-'
            },
            {
                '-', '-', 'x', '-', '2', '-', '-'
            },
            {
                '-', '-', '-', 'x', '-', '-', '-'
            }
        };
        akariProblems.add(problem);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        TabbedPane = new javax.swing.JTabbedPane();
        AkariPanel = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextPane3 = new javax.swing.JTextPane();
        boardTableScrollPane = new javax.swing.JScrollPane();
        boardTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        examplesBox = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        akariConsoleTextArea = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        colsTextField = new javax.swing.JFormattedTextField();
        rowsTextField = new javax.swing.JFormattedTextField();
        JobShopPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        machineNrTextField = new javax.swing.JFormattedTextField();
        taskNrTextField = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jsTimeTable = new javax.swing.JTable();
        jScrollPane7 = new javax.swing.JScrollPane();
        jsOrderTable = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jsConsoleTextArea = new javax.swing.JTextArea();
        TSPPanel = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextPane4 = new javax.swing.JTextPane();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tspTimesTable = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tspLimitTable = new javax.swing.JTable();
        jScrollPane11 = new javax.swing.JScrollPane();
        tspLimitCostTable = new javax.swing.JTable();
        townNrTextField = new javax.swing.JFormattedTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        tspSoftConditionsBox = new javax.swing.JCheckBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        tspTexyArea = new javax.swing.JTextArea();
        starttownTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        comparatorBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        inDomainBox = new javax.swing.JComboBox();
        solveButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                TabbedPaneStateChanged(evt);
            }
        });

        jTextPane3.setEditable(false);
        jTextPane3.setBackground(new java.awt.Color(240, 240, 240));
        jTextPane3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Opis", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jTextPane3.setText("Akari (jap. światło), znane także jako Light Up (ang. oświetl, rozjaśnij) – łamigłówka logiczna, wprowadzona na rynek w 2001 roku przez wydawnictwo Nikoli.\n\nGra rozgrywa się na prostokątnej siatce, na której znajdują się białe i czarne pola; w niektórych czarnych polach znajdują się cyfry od 0 do 4. Zadaniem gracza jest podświetlenie wszystkich białych pól. Pola podświetla się żarówkami, które podświetlają pola poziomo i pionowo, aż natrafią na czarną kratkę. Żarówki nie mogą świecić na siebie, to znaczy nie mogą znajdować się w tym samym rzędzie lub tej samej kolumnie, jeśli nie przedziela je czarne pole. Cyfry w kratkach wskazują, ile żarówek musi stykać się bokiem z kratką zawierającą cyfrę. Żarówki można umieszczać jednak nie tylko przy kratkach, ale i w dowolnym białym polu.");
        jScrollPane4.setViewportView(jTextPane3);

        boardTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String []
            {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"
            }
        ));
        boardTable.addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentResized(java.awt.event.ComponentEvent evt)
            {
                boardTableComponentResized(evt);
            }
        });
        boardTableScrollPane.setViewportView(boardTable);
        boardTable.getAccessibleContext().setAccessibleName("table");

        jLabel1.setText("Wczytaj zadanie");

        examplesBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                examplesBoxActionPerformed(evt);
            }
        });

        akariConsoleTextArea.setColumns(20);
        akariConsoleTextArea.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        akariConsoleTextArea.setRows(5);
        akariConsoleTextArea.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Konsola", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jScrollPane2.setViewportView(akariConsoleTextArea);
        akariConsoleTextArea.getAccessibleContext().setAccessibleName("consoleTextView");

        jLabel4.setText("Liczba kolumn");

        jLabel5.setText("Liczba wierszy");

        colsTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        colsTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                colsTextFieldActionPerformed(evt);
            }
        });
        colsTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                colsTextFieldFocusLost(evt);
            }
        });

        rowsTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        rowsTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rowsTextFieldActionPerformed(evt);
            }
        });
        rowsTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                rowsTextFieldFocusLost(evt);
            }
        });

        javax.swing.GroupLayout AkariPanelLayout = new javax.swing.GroupLayout(AkariPanel);
        AkariPanel.setLayout(AkariPanelLayout);
        AkariPanelLayout.setHorizontalGroup(
            AkariPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AkariPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AkariPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AkariPanelLayout.createSequentialGroup()
                        .addGroup(AkariPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(boardTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(11, 11, 11))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AkariPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(examplesBox, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rowsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(colsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        AkariPanelLayout.setVerticalGroup(
            AkariPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AkariPanelLayout.createSequentialGroup()
                .addGroup(AkariPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AkariPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(AkariPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(examplesBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(colsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(rowsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(boardTableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE))
                .addContainerGap())
        );

        examplesBox.getAccessibleContext().setAccessibleName("chooseExampleBox");

        TabbedPane.addTab("Akari", AkariPanel);

        jLabel6.setText("Liczba maszyn");

        machineNrTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        machineNrTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                machineNrTextFieldActionPerformed(evt);
            }
        });
        machineNrTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                machineNrTextFieldFocusLost(evt);
            }
        });

        taskNrTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        taskNrTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                taskNrTextFieldActionPerformed(evt);
            }
        });
        taskNrTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                taskNrTextFieldFocusLost(evt);
            }
        });

        jLabel7.setText("Liczba zlecen");

        jsTimeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String []
            {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"
            }
        ));
        jScrollPane6.setViewportView(jsTimeTable);

        jsOrderTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String []
            {
                "Kolejność na maszynach"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }
        });
        jsOrderTable.setToolTipText("");
        jScrollPane7.setViewportView(jsOrderTable);

        jsConsoleTextArea.setColumns(20);
        jsConsoleTextArea.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        jsConsoleTextArea.setRows(5);
        jsConsoleTextArea.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Konsola", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jScrollPane5.setViewportView(jsConsoleTextArea);

        javax.swing.GroupLayout JobShopPanelLayout = new javax.swing.GroupLayout(JobShopPanel);
        JobShopPanel.setLayout(JobShopPanelLayout);
        JobShopPanelLayout.setHorizontalGroup(
            JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JobShopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5)
                    .addGroup(JobShopPanelLayout.createSequentialGroup()
                        .addGroup(JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JobShopPanelLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(machineNrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(taskNrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(JobShopPanelLayout.createSequentialGroup()
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 601, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 17, Short.MAX_VALUE)))
                .addContainerGap())
        );
        JobShopPanelLayout.setVerticalGroup(
            JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JobShopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(taskNrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7))
                    .addGroup(JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(machineNrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(JobShopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                .addContainerGap())
        );

        TabbedPane.addTab("JobShop", JobShopPanel);

        jTextPane4.setEditable(false);
        jTextPane4.setBackground(new java.awt.Color(240, 240, 240));
        jTextPane4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Opis", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jTextPane4.setText("Problem komiwojażera (TSP - ang. travelling salesman problem) jest to zagadnienie optymalizacyjne, polegające na znalezieniu minimalnego cyklu Hamiltona w pełnym grafie ważonym.\n\nNazwa pochodzi od typowej ilustracji problemu, przedstawiającej go z punktu widzenia wędrownego sprzedawcy (komiwojażera): dane jest n miast, które komiwojażer ma odwiedzić, oraz czas podróży pomiędzy każdą parą miast. Celem jest znalezienie najkrótszej/najtańszej/najszybszej drogi łączącej wszystkie miasta zaczynającej się i kończącej się w określonym punkcie.\n\nZadanie zosrtało rozrzerzone do problemu komiwojażera z oknami czasowymi (TSP-TW - ang. travelling salesman problem with time windows). Okna te moga być traktowane jako ograniczenie twarde bądź ograniczenie miekkie (nie muszą być spełnione, ale ich niespełnienie zaniża ocenę rozwiązania). Możliwe jest określenie kosztów niespełnienia danego ograniczenia.");
        jScrollPane8.setViewportView(jTextPane4);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Czasy podóży"));

        tspTimesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String []
            {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"
            }
        ));
        jScrollPane9.setViewportView(tspTimesTable);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Limity czasowe"));

        tspLimitTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String []
            {
                "Title 1"
            }
        ));
        jScrollPane10.setViewportView(tspLimitTable);

        tspLimitCostTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String []
            {
                "Title 1"
            }
        ));
        jScrollPane11.setViewportView(tspLimitCostTable);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        townNrTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        townNrTextField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                townNrTextFieldActionPerformed(evt);
            }
        });
        townNrTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                townNrTextFieldFocusLost(evt);
            }
        });

        jLabel9.setText("Liczba miast");

        jLabel10.setText("Miasto startowe");

        tspSoftConditionsBox.setText("Ograniczenia miękkie");
        tspSoftConditionsBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                tspSoftConditionsBoxActionPerformed(evt);
            }
        });

        tspTexyArea.setColumns(20);
        tspTexyArea.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        tspTexyArea.setRows(5);
        tspTexyArea.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Konsola", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jScrollPane3.setViewportView(tspTexyArea);

        starttownTextField.setText("jTextField1");
        starttownTextField.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusLost(java.awt.event.FocusEvent evt)
            {
                starttownTextFieldFocusLost(evt);
            }
        });

        javax.swing.GroupLayout TSPPanelLayout = new javax.swing.GroupLayout(TSPPanel);
        TSPPanel.setLayout(TSPPanelLayout);
        TSPPanelLayout.setHorizontalGroup(
            TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TSPPanelLayout.createSequentialGroup()
                .addGroup(TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(TSPPanelLayout.createSequentialGroup()
                        .addGroup(TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(TSPPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(townNrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(starttownTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tspSoftConditionsBox)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(TSPPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        TSPPanelLayout.setVerticalGroup(
            TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TSPPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(TSPPanelLayout.createSequentialGroup()
                        .addGroup(TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(starttownTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9)
                                .addComponent(townNrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel10)
                                .addComponent(tspSoftConditionsBox)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(TSPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE))
                .addContainerGap())
        );

        TabbedPane.addTab("Problem komiwojażera", TSPPanel);

        jLabel2.setText("Comparator");

        comparatorBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                comparatorBoxActionPerformed(evt);
            }
        });

        jLabel3.setText("InDomain");

        inDomainBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                inDomainBoxActionPerformed(evt);
            }
        });

        solveButton.setText("Rozwiąż");
        solveButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                solveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(TabbedPane)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comparatorBox, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(inDomainBox, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(solveButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(comparatorBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(inDomainBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(solveButton))
                .addGap(13, 13, 13)
                .addComponent(TabbedPane))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("empty-statement")
    private void prepareView()
    {
        //search parameters
        Object[] acomparators =
        {
            new LargestDomain(), new LargestMax(), new LargestMin(), /*new MaxCardDiff(),
             new MaxGlbCard(), new MaxLubCard(), new MaxRegret(), new MinCardDiff(),
             new MinDomainOverDegree(), new MinGlbCard(), new MinLubCard(),*/
            new MostConstrainedDynamic(), new MostConstrainedStatic(),
            new SmallestDomain(), new SmallestMax(), new SmallestMin(), new WeightedDegree()
        };

        Object[] ainDomains =
        {
            new IndomainMin(), new IndomainMedian(), new IndomainMax(),
            new IndomainMedian(), new IndomainRandom()
        };

        comparators = acomparators;
        inDomains = ainDomains;

        for (int i = 0; i < Array.getLength(comparators); i++)
        {
            comparatorBox.addItem(comparators[i].getClass().getName());
        }
        for (int i = 0; i < Array.getLength(inDomains); i++)
        {
            inDomainBox.addItem(inDomains[i].getClass().getName());
        }

        //akari board
        boardTable.setModel(new AkariTable.Model(5, 5));
        boardTable.setDefaultEditor(String.class, new AkariTable.Editor(boardTable));
        boardTable.setDefaultRenderer(String.class, new AkariTable.Renderer());
        boardTable.setTableHeader(null);
        boardTableScrollPane.setColumnHeaderView(null);
        boardTable.getModel().addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent tme)
            {
                // adjustCellColors();
            }
        });
        akariDescriptionToTable(akariDescription);

        //akari combo box
        for (int i = 0; i < akariProblems.size(); i++)
        {
            examplesBox.addItem("Zadanie " + (i + 1));
        }

        /**
         * ********************** Job shop **********************************
         */
        int machines = 5;
        int tasks = 5;
        String[] colNames =
        {
            "", "Start", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
        };
        ResizableTableModel model = new ResizableTableModel(tasks, machines + 2);
        model.columnNames = colNames;
        for (int i = 0; i < tasks; i++)
        {
            for (int j = 0; j < machines + 1; j++)
            {
                model.setValueAt("0", i, j + 1);
            }
        }
        jsTimeTable.setModel(model);
        jsTimeTable.setDefaultEditor(String.class, new JobShopTableEditor(jsTimeTable));

        model = new ResizableTableModel(tasks, 1);
        model.columnNames = new String[1];
        model.columnNames[0] = "Kolejnosć na maszynach";
        jsOrderTable.setModel(model);
        setMachinesNr(machines);
        setTasksNr(tasks);

        setJobShopExample();

        /**
         * ********************** TSP **********************************
         */
        model = new ResizableTableModel(1, 2);
        String[] tspColNames =
        {
            "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
        };
        model.columnNames = tspColNames;
        tspTimesTable.setModel(model);
        
        String[] colName = {"Limit"};
        model = new ResizableTableModel(1, 1);
        model.columnNames = colName;
        tspLimitTable.setModel(model);
        
        String[] colName2 = {"Kara"};
        model = new ResizableTableModel(1, 1);
        model.columnNames = colName2;
        tspLimitCostTable.setModel(model);

        setTSPExample();

    }

    private void akariTableDataToDescription()
    {
        AkariModel.Description problem = new AkariModel.Description();
        problem.nrOfCols = boardTable.getColumnCount();
        problem.nrOfRows = boardTable.getRowCount();
        problem.board = new char[problem.nrOfRows][problem.nrOfCols];

        for (int row = 0; row < boardTable.getRowCount(); row++)
        {
            for (int col = 0; col < boardTable.getColumnCount(); col++)
            {
                String val = (String) boardTable.getValueAt(row, col);
                char c;

                if (val == null || val.charAt(0) == ' ')
                {
                    c = '-';
                }
                else
                {
                    c = val.charAt(0);
                }

                problem.board[row][col] = c;
            }
        }

        akariDescription = new AkariModel.Description(problem);
    }

    private void akariDescriptionToTable(AkariModel.Description problem)
    {
        AkariTable.Model model = (AkariTable.Model) boardTable.getModel();
        model.setColumnNumber(problem.nrOfCols);
        model.setRowsNumber(problem.nrOfRows);

        for (int row = 0; row < problem.nrOfRows; row++)
        {
            for (int col = 0; col < problem.nrOfCols; col++)
            {
                char c = problem.board[row][col];
                String val;

                if (c == '-')
                {
                    val = null;
                }
                else
                {
                    val = Character.toString(c);
                }

                model.setValueAt(val, row, col);
            }
        }

        rowsTextField.setText(Integer.toString(problem.nrOfRows));
        colsTextField.setText(Integer.toString(problem.nrOfCols));
    }

    private void setJobShopExample()
    {
        JobShopModel.Description d = new JobShopModel.Description();
        d.machines = new ArrayList(Arrays.asList("A", "B", "C", "D"));
        d.tasks = new ArrayList();
        d.tasks.add(new JobShopModel.Task("Zlecenie 1", new ArrayList(Arrays.asList(30, 60, 2, 5)), new ArrayList(Arrays.asList(1, 0, 2, 3)), 0));
        d.tasks.add(new JobShopModel.Task("Zlecenie 2", new ArrayList(Arrays.asList(75, 25, 3, 10)), new ArrayList(Arrays.asList(0, 2, 1, 3)), 15));
        d.tasks.add(new JobShopModel.Task("Zlecenie 3", new ArrayList(Arrays.asList(15, 10, 5, 30)), new ArrayList(Arrays.asList(2, 0, 1, 3)), 15));
        d.tasks.add(new JobShopModel.Task("Zlecenie 4", new ArrayList(Arrays.asList(1, 1, 1, 90)), new ArrayList(Arrays.asList(3, 1, 0, 2)), 60));

        setMachinesNr(d.machines.size());
        setTasksNr(d.tasks.size());
        ResizableTableModel model = (ResizableTableModel) jsTimeTable.getModel();

        for (int i = 0; i < d.tasks.size(); i++)
        {
            JobShopModel.Task task = d.tasks.get(i);

            String orderStr = "";

            model.setValueAt(Integer.toString(task.startTime), i, 1);
            for (int j = 0; j < d.machines.size(); j++)
            {
                model.setValueAt("" + task.durtaions.get(j), i, j + 2);
            }

            for (int machine : task.order)
            {
                orderStr += d.machines.get(machine) + ", ";
            }
            orderStr = orderStr.substring(0, orderStr.length() - 2);

            jsOrderTable.setValueAt(orderStr, i, 0);
        }
    }
    
    private void setTSPExample()
    {
                int[][] distance =
        {
            {
                1000, 85, 110, 94, 71, 76, 25, 56, 94, 67
            },
            {
                85, 1000, 26, 70, 62, 60, 63, 62, 70, 49
            },
            {
                110, 26, 1000, 71, 87, 89, 88, 87, 93, 73
            },
            {
                94, 70, 71, 1000, 121, 19, 82, 106, 124, 105
            },
            {
                71, 62, 87, 121, 1000, 104, 53, 24, 8, 13
            },
            {
                76, 60, 89, 19, 104, 1000, 65, 89, 108, 93
            },
            {
                25, 63, 88, 82, 53, 65, 1000, 30, 57, 46
            },
            {
                56, 62, 87, 106, 24, 89, 30, 1000, 23, 20
            },
            {
                94, 70, 93, 124, 8, 108, 57, 23, 1000, 20
            },
            {
                67, 49, 73, 105, 13, 93, 46, 20, 20, 1000
            }
        };

        int[] aLimit =
        {
            400, 400, 400, 400, 72, 400, 400, 400, 400, 400
        };

        TSPTWModel.Description description = new TSPTWModel.Description();
        description.noCities = 10;
        description.distance = distance;
        description.limit = aLimit;
        description.startCity = 1;
        
        setTownsNr(description.noCities);
        starttownTextField.setText(Character.toString((char) ('A' + description.startCity-1)));
        
        ResizableTableModel model = (ResizableTableModel) tspTimesTable.getModel();
        ResizableTableModel model2 = (ResizableTableModel) tspLimitTable.getModel();
        ResizableTableModel model3 = (ResizableTableModel) tspLimitCostTable.getModel();
        for(int i = 0; i < description.noCities; i++)
        {
            for(int j = 0; j < description.noCities; j++)
            {
                if(i == j)
                    continue;
                
                model.setValueAt(""+description.distance[i][j], i, j+1);
            }
            
            model2.setValueAt(""+description.limit[i], i, 0);
           // model3.setValueAt(aLimit, i, i);
        }
        
        
        
    }
    

    private void adjustCellSize()
    {
        int rowHeight = boardTableScrollPane.getHeight() / boardTable.getRowCount() - boardTable.getRowMargin();
        boardTable.setRowHeight(rowHeight);
    }

    private void adjustCellNr()
    {
        int nr = akariDescription.nrOfCols;
        ResizableTableModel model = (ResizableTableModel) boardTable.getModel();
        model.setColumnNumber(nr);
    }

    private void adjustRowsNr()
    {
        int nr = akariDescription.nrOfRows;
        ResizableTableModel model = (ResizableTableModel) boardTable.getModel();
        model.setRowsNumber(nr);
    }


    private void examplesBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_examplesBoxActionPerformed
    {//GEN-HEADEREND:event_examplesBoxActionPerformed
        int i = examplesBox.getSelectedIndex();

        akariDescription = new AkariModel.Description(akariProblems.get(i));
        akariDescriptionToTable(akariDescription);
    }//GEN-LAST:event_examplesBoxActionPerformed

    private void comparatorBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_comparatorBoxActionPerformed
    {//GEN-HEADEREND:event_comparatorBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comparatorBoxActionPerformed

    private void inDomainBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_inDomainBoxActionPerformed
    {//GEN-HEADEREND:event_inDomainBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inDomainBoxActionPerformed

    private void solveButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_solveButtonActionPerformed
    {//GEN-HEADEREND:event_solveButtonActionPerformed
        // TODO add your handling code here:
        ComparatorVariable comparator = (ComparatorVariable) comparators[comparatorBox.getSelectedIndex()];
        Indomain indomain = (Indomain) inDomains[inDomainBox.getSelectedIndex()];

        Solver s = new Solver(comparator, indomain);

        if (TabbedPane.getSelectedComponent() == AkariPanel)
        {
            akariTableDataToDescription();
            AkariModel model = new AkariModel();
            model.description = new AkariModel.Description(akariDescription);
            model.prepare();

            s.solve(model.getStore(), model.getVariablesList());

            akariDescriptionToTable(model.getResult());
        }
        else if (TabbedPane.getSelectedComponent() == JobShopPanel)
        {
            JobShopModel model = new JobShopModel();
            try
            {
                model.description = parseJobShopProblem();
                model.prepare();
            } catch (Exception e)
            {
                System.err.println(e);
            }

            s.solve(model.getStore(), model.getVariablesList(), model.getCost());

            IntVar[][] startTimes = model.getResult();

            ChartPanel chartPanel = new ChartPanel(createChart(createDataset(startTimes, model.description.tasks, model.description.machines)));
            showChart(chartPanel);
        }
        else if (TabbedPane.getSelectedComponent() == TSPPanel)
        {
            TSPTWModel.Description d = parseTSPProblem();
            TSPTWModel model = new TSPTWModel();
            model.description = d;
            model.prepare();
            
            s.solve(model.getStore(), model.getVariablesList(), model.getCost());
            
            String orderStr = "";
            for(int city : model.getResult())
                orderStr += Character.toString((char) (city + 'A' -1)) + " ";
            
            System.out.println("Kolejnosć odwiedzania miast: "+orderStr);
        }
    }//GEN-LAST:event_solveButtonActionPerformed

    private void colsTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_colsTextFieldActionPerformed
    {//GEN-HEADEREND:event_colsTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_colsTextFieldActionPerformed

    private void rowsTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rowsTextFieldActionPerformed
    {//GEN-HEADEREND:event_rowsTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rowsTextFieldActionPerformed

    private void machineNrTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_machineNrTextFieldActionPerformed
    {//GEN-HEADEREND:event_machineNrTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_machineNrTextFieldActionPerformed

    private void taskNrTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_taskNrTextFieldActionPerformed
    {//GEN-HEADEREND:event_taskNrTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_taskNrTextFieldActionPerformed

    private void townNrTextFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_townNrTextFieldActionPerformed
    {//GEN-HEADEREND:event_townNrTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_townNrTextFieldActionPerformed

    private void tspSoftConditionsBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_tspSoftConditionsBoxActionPerformed
    {//GEN-HEADEREND:event_tspSoftConditionsBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tspSoftConditionsBoxActionPerformed

    private void boardTableComponentResized(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_boardTableComponentResized
    {//GEN-HEADEREND:event_boardTableComponentResized
        adjustCellSize();
    }//GEN-LAST:event_boardTableComponentResized

    private void rowsTextFieldFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_rowsTextFieldFocusLost
    {//GEN-HEADEREND:event_rowsTextFieldFocusLost
        try
        {
            int nr = Integer.parseInt(rowsTextField.getText());
            if (nr > 0)
            {
                akariDescription.nrOfRows = Math.min(nr, 20);
            }

        } catch (NumberFormatException e)
        {
        }
        rowsTextField.setText(Integer.toString(akariDescription.nrOfRows));
        adjustRowsNr();

    }//GEN-LAST:event_rowsTextFieldFocusLost

    private void colsTextFieldFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_colsTextFieldFocusLost
    {//GEN-HEADEREND:event_colsTextFieldFocusLost
        try
        {
            int nr = Integer.parseInt(colsTextField.getText());
            if (nr > 0)
            {
                akariDescription.nrOfCols = Math.min(nr, 20);
            }

        } catch (NumberFormatException e)
        {
        }
        colsTextField.setText(Integer.toString(akariDescription.nrOfCols));
        adjustCellNr();
    }//GEN-LAST:event_colsTextFieldFocusLost

    private void machineNrTextFieldFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_machineNrTextFieldFocusLost
    {//GEN-HEADEREND:event_machineNrTextFieldFocusLost
        int nr = jsTimeTable.getColumnCount() - 2;
        try
        {
            nr = Integer.parseInt(machineNrTextField.getText());
            nr = (nr < 1) ? 1 : Math.min(nr, 10);
        } catch (NumberFormatException e)
        {
        }

        setMachinesNr(nr);
    }//GEN-LAST:event_machineNrTextFieldFocusLost

    private void taskNrTextFieldFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_taskNrTextFieldFocusLost
    {//GEN-HEADEREND:event_taskNrTextFieldFocusLost
        int nr = jsTimeTable.getRowCount();
        try
        {
            nr = Integer.parseInt(taskNrTextField.getText());
            nr = (nr < 1) ? 1 : Math.min(nr, 10);
        } catch (NumberFormatException e)
        {
        }

        setTasksNr(nr);
    }//GEN-LAST:event_taskNrTextFieldFocusLost

    private void townNrTextFieldFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_townNrTextFieldFocusLost
    {//GEN-HEADEREND:event_townNrTextFieldFocusLost
        // TODO add your handling code here:
        int nr = tspLimitCostTable.getRowCount();
        int val = nr;

        if (StringUtilities.isNumeric((String) townNrTextField.getText()))
        {
            val = (int) Double.parseDouble((String) townNrTextField.getText());

            if (val < 2)
            {
                val = nr;
            }
            else
            {
                val = Math.min(val, 10);
            }
        }

        setTownsNr(val);
    }//GEN-LAST:event_townNrTextFieldFocusLost

    private void starttownTextFieldFocusLost(java.awt.event.FocusEvent evt)//GEN-FIRST:event_starttownTextFieldFocusLost
    {//GEN-HEADEREND:event_starttownTextFieldFocusLost
        String val = starttownTextField.getText();
        val = val.trim();
        val = val.toUpperCase();
        
        int nrOfTowns = tspLimitTable.getRowCount();
        char lastTownId = (char) ('A' + nrOfTowns - 1);

        if ((val.length() != 1) || (val.charAt(0) > lastTownId) || (val.charAt(0) < 'A'))
        {
            val = "A";
        }

        starttownTextField.setText(val);
    }//GEN-LAST:event_starttownTextFieldFocusLost

    private void TabbedPaneStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_TabbedPaneStateChanged
    {//GEN-HEADEREND:event_TabbedPaneStateChanged
        // TODO add your handling code here:
        MessageConsole mc = null;
        
        if(TabbedPane.getSelectedComponent() == AkariPanel)
        {
            mc = messageConsoleAkari;
        }
        else if(TabbedPane.getSelectedComponent() == JobShopPanel)
        {
            mc = messageConsoleJobShop;
        }
        else if(TabbedPane.getSelectedComponent() == TSPPanel)
        {
            mc = messageConsoleTSP;
        }
        
        if(mc != null)
        {
            mc.redirectOut();
            mc.redirectErr(Color.red, null);
        }
    }//GEN-LAST:event_TabbedPaneStateChanged

    private void setTasksNr(int nr)
    {
        taskNrTextField.setText(Integer.toString(nr));
        ResizableTableModel m = (ResizableTableModel) jsTimeTable.getModel();
        m.setRowsNumber(nr);

        for (int i = 0; i < nr; i++)
        {
            m.setValueAt("Zlecenie " + (i + 1), i, 0);
            m.disableCell(i, 0);
        }
        fillWithZeros((AbstractTableModel) m);

        m = (ResizableTableModel) jsOrderTable.getModel();
        m.setRowsNumber(nr);
    }

    private void setMachinesNr(int nr)
    {
        machineNrTextField.setText(Integer.toString(nr));
        ((ResizableTableModel) jsTimeTable.getModel()).setColumnNumber(nr + 2);
        fillWithZeros((AbstractTableModel) jsTimeTable.getModel());
    }

    private void fillWithZeros(AbstractTableModel model)
    {
        for (int i = 0; i < model.getRowCount(); i++)
        {
            for (int j = 0; j < model.getColumnCount(); j++)
            {
                if (model.getValueAt(i, j) == null)
                {
                    model.setValueAt("0", i, j);
                }
            }
        }
    }

    private JobShopModel.Description parseJobShopProblem() throws Exception
    {
        JobShopModel.Description description = new JobShopModel.Description();
        description.machines = new ArrayList();
        description.tasks = new ArrayList();

        ResizableTableModel m = (ResizableTableModel) jsTimeTable.getModel();
        ResizableTableModel m2 = (ResizableTableModel) jsOrderTable.getModel();
        int nrOfMachines = m.getColumnCount() - 2;
        int nrOfTasks = m.getRowCount();

        //get machines names
        for (int i = 0; i < nrOfMachines; i++)
        {
            description.machines.add(m.getColumnName(i + 2));
        }

        //get tasks
        for (int i = 0; i < nrOfTasks; i++)
        {
            JobShopModel.Task task = new JobShopModel.Task((String) m.getValueAt(i, 0));

            int start = 0;
            try
            {
                start = Integer.parseInt((String) jsTimeTable.getValueAt(i, 1));
                start = Math.max(0, start);
            } catch (NumberFormatException e)
            {
            }
            task.startTime = start;
            task.durtaions = new ArrayList();
            task.order = new ArrayList();

            for (int j = 0; j < nrOfMachines; j++)
            {
                int nrVal = 0;
                String val = (String) m.getValueAt(i, j + 2);

                if (val != null)
                {
                    try
                    {
                        nrVal = Integer.parseInt(val);
                        nrVal = Math.max(0, nrVal);
                    } catch (NumberFormatException e)
                    {
                    }
                }

                task.durtaions.add(nrVal);
            }

            //parse order
            String orderStr = (String) m2.getValueAt(i, 0);
            if (orderStr == null)
            {
                throw new Exception("Order for " + task.name + " undefined");
            }

            String[] parts = orderStr.split(",");
            for (String machine : parts)
            {
                machine = machine.trim();

                if (!description.machines.contains(machine))
                {
                    throw new Exception("Incorrect problem description: unknown machine \"" + machine + "\" in order description of " + task.name);
                }

                task.order.add(description.machines.indexOf(machine));
            }

            //add task
            description.tasks.add(task);
        }

        return description;
    }

    void setTownsNr(int nr)
    {
        //time table
        ResizableTableModel model = (ResizableTableModel) tspTimesTable.getModel();
        model.setColumnNumber(nr + 1);
        model.setRowsNumber(nr);
        for (int i = 0; i < nr; i++)
        {
            model.setValueAt(Character.toString((char) ('A' + i)), i, 0);
            model.disableCell(i, 0);

            model.setValueAt("x", i, i + 1);
            model.disableCell(i, i + 1);
        }
        fillWithZeros(model);

        //limit table
        model = (ResizableTableModel) tspLimitTable.getModel();
        model.setRowsNumber(nr);
        for (int i = 0; i < nr; i++)
        {
            if (model.getValueAt(i, 0) == null)
            {
                model.setValueAt("1000", i, 0);
            }
        }

        //limit cost table
        model = (ResizableTableModel) tspLimitCostTable.getModel();
        model.setRowsNumber(nr);
        for (int i = 0; i < nr; i++)
        {
            if (model.getValueAt(i, 0) == null)
            {
                model.setValueAt("0", i, 0);
            }
        }

        townNrTextField.setText("" + nr);
        
        int startTown = starttownTextField.getText().charAt(0) - 'A';
        if(startTown >= nr)
        {
            startTown = nr-1;
        }
        starttownTextField.setText(Character.toString((char) ('A'+startTown)));
    }
    private TSPTWModel.Description parseTSPProblem()
    {
        TSPTWModel.Description d = new TSPTWModel.Description();
       
        d.noCities = tspTimesTable.getRowCount();
        d.startCity = starttownTextField.getText().charAt(0) - 'A' + 1;
        d.distance = new int[d.noCities][d.noCities];
        
        ResizableTableModel model = (ResizableTableModel) tspTimesTable.getModel();
        ResizableTableModel model2 = (ResizableTableModel) tspLimitTable.getModel();
        ResizableTableModel model3 = (ResizableTableModel) tspLimitCostTable.getModel();
        
        int[] limits = new int[d.noCities];
        int[] limitCosts = new int[d.noCities];
        
        for(int i = 0; i < d.noCities; i++)
        {
            for(int j = 0; j < d.noCities; j++)
            {
                if(i == j)
                    d.distance[i][j] = 10000;
                else
                {
                    d.distance[i][j] = Integer.parseInt((String) model.getValueAt(i, j+1));
                }
            }
            
            limits[i] = Integer.parseInt((String) model2.getValueAt(i, 0));
            limitCosts[i] = Integer.parseInt((String) model3.getValueAt(i, 0));
        }
        
        if(!tspSoftConditionsBox.isSelected())
        {
            limitCosts = null;
        }
        
        d.limit = limits;
        d.limitCost = limitCosts;
        
        return d;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new MainFrame().setVisible(true);
            }
        });
    }

    public static class JobShopTableEditor extends DefaultCellEditor
    {

        private final JTable table;
        private final JTextField field;

        public JobShopTableEditor(JTable table)
        {
            super(new JTextField());
            this.field = (JTextField) this.getComponent();
            this.table = table;
        }

        @Override
        public boolean stopCellEditing()
        {
            String val = field.getText();

            if (!StringUtilities.isNumeric(val))
            {
                val = null;
            }
            else
            {
                int nr = (int) Double.parseDouble(val);
                nr = Math.max(0, nr);
                val = Integer.toString(nr);
            }

            field.setText(val);

            return super.stopCellEditing();
        }
    }

    private JFreeChart createChart(final IntervalCategoryDataset dataset)
    {
        final JFreeChart chart = ChartFactory.createGanttChart(
                "Wykres Gantta", // chart title
                "Zadanie", // domain axis label
                "Czas", // range axis label
                dataset, // data
                true, // include legend
                true, // tooltips
                false // urls
        );
        DateAxis axis = (DateAxis) ((CategoryPlot) chart.getPlot()).getRangeAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("S"));

        IntervalXYDataset d = new XYTaskDataset((TaskSeriesCollection) dataset);

        //JFreeChart chart = ChartFactory.createXYBarChart("Wykres gantta", "Czas", false, "Zadanie", d, PlotOrientation.HORIZONTAL, true, false, false);
        //JFreeChart chart = ChartFactory.createXYAreaChart("Wyrkes gantta", "", "", d, PlotOrientation.HORIZONTAL, true, false, false);
//        JFreeChart localJFreeChart = ChartFactory.createXYBarChart("XYTaskDatasetDemo1", "Resource", false, "Timing", d, PlotOrientation.HORIZONTAL, true, false, false);
//        localJFreeChart.setBackgroundPaint(Color.white);
//        XYPlot localXYPlot = (XYPlot) localJFreeChart.getPlot();
//        SymbolAxis localSymbolAxis = new SymbolAxis("Series", new String[]
//        {
//            "Team A", "Team B", "Team C", "Team D"
//        });
//        localSymbolAxis.setGridBandsVisible(false);
//        localXYPlot.setDomainAxis(localSymbolAxis);
//        XYBarRenderer localXYBarRenderer = (XYBarRenderer) localXYPlot.getRenderer();
//        localXYBarRenderer.setUseYInterval(true);
//        localXYPlot.setRangeAxis(new DateAxis("Timing"));
//        ChartUtilities.applyCurrentTheme(localJFreeChart);
//        return localJFreeChart;
        return chart;
    }

    private void showChart(ChartPanel chartPanel)
    {
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new BorderLayout());
        jPanel4.add(chartPanel, BorderLayout.NORTH);

        JFrame frame = new JFrame();
        frame.add(jPanel4);
        frame.pack();
        frame.setVisible(true);
        //JobShopPanel.add(chartPanel);
    }

    public static IntervalCategoryDataset createDataset(IntVar[][] startTimes, ArrayList<JobShopModel.Task> tasks, ArrayList<String> machines)
    {
        final TaskSeriesCollection collection = new TaskSeriesCollection();

        int i = 0;
        for (JobShopModel.Task task : tasks)
        {
            TaskSeries s = new TaskSeries(task.name);

            int j = 0;
            for (int duration : task.durtaions)
            {
                int start = startTimes[i][j].value();
                int end = start + duration;

                s.add(new Task(machines.get(j), new SimpleTimePeriod(start, end)));

                j++;
            }

            collection.add(s);

            i++;
        }

        return collection;
    }

    private static Date date(final int day, final int month, final int year)
    {

        final Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        final Date result = calendar.getTime();
        return result;

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel AkariPanel;
    private javax.swing.JPanel JobShopPanel;
    private javax.swing.JPanel TSPPanel;
    private javax.swing.JTabbedPane TabbedPane;
    private javax.swing.JTextArea akariConsoleTextArea;
    private javax.swing.JTable boardTable;
    private javax.swing.JScrollPane boardTableScrollPane;
    private javax.swing.JFormattedTextField colsTextField;
    private javax.swing.JComboBox comparatorBox;
    private javax.swing.JComboBox examplesBox;
    private javax.swing.JComboBox inDomainBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTextPane jTextPane3;
    private javax.swing.JTextPane jTextPane4;
    private javax.swing.JTextArea jsConsoleTextArea;
    private javax.swing.JTable jsOrderTable;
    private javax.swing.JTable jsTimeTable;
    private javax.swing.JFormattedTextField machineNrTextField;
    private javax.swing.JFormattedTextField rowsTextField;
    private javax.swing.JButton solveButton;
    private javax.swing.JTextField starttownTextField;
    private javax.swing.JFormattedTextField taskNrTextField;
    private javax.swing.JFormattedTextField townNrTextField;
    private javax.swing.JTable tspLimitCostTable;
    private javax.swing.JTable tspLimitTable;
    private javax.swing.JCheckBox tspSoftConditionsBox;
    private javax.swing.JTextArea tspTexyArea;
    private javax.swing.JTable tspTimesTable;
    // End of variables declaration//GEN-END:variables
}
