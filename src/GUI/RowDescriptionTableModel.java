/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import Utils.ResizableTableModel;

/**
 *
 * @author slawus
 */
public class RowDescriptionTableModel extends ResizableTableModel
{
    String[] labels;
    
    
    public RowDescriptionTableModel(int rows, int cols)
    {
        super(rows, cols);
        
        labels = new String[rows];
    }

    public void setLabel(String text, int row)
    {
        labels[row] = text;
    }

    @Override
    public void addRow(int row)
    {
        String[] l = new String[rows+1];
        System.arraycopy(labels, 0, l, 0, row-1);
        
        super.addColumn(row); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    @Override
    public boolean isCellEditable(int row, int col)
    {
        if(col == 0)
            return false;
        
        return super.isCellEditable(row, col); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int row, int col)
    {
        if(col == 0)
        {
            return labels[row];
        }
        
        return super.getValueAt(row, col-1); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
