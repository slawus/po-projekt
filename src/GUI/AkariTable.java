/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Utils.ResizableTableModel;
import Utils.StringUtilities;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import Utils.StringUtilities;
import java.awt.Color;
import java.awt.Component;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author slawus
 */
public class AkariTable extends JTable
{

    public static class Editor extends DefaultCellEditor
    {

        private final JTable table;
        private final Model model;
        private final JTextField field;
        public Editor(JTable table)
        {
            super(new JTextField());
            this.field = (JTextField) this.getComponent();
            this.table = table;
            this.model = (Model) table.getModel();
        }

        @Override
        public boolean stopCellEditing()
        {
            String val = field.getText();

            val = parse(val);
            field.setText(val);

            return super.stopCellEditing();
        }

        String parse(String val)
        {
            if (val == null)
            {
                return null;
            }

            if (StringUtilities.isNumeric(val))
            {
                double numericVal = Double.parseDouble(val);

                if (numericVal <= 4 && numericVal >= 0)
                {
                    return Integer.toString((int) numericVal);
                }
            }
            
            if(val.equals("*") && model.isLighted(table.getEditingRow(), table.getEditingColumn()))
                return null;
            
            if (val.equals("*") || val.equals("x"))
            {
                return val;
            }

            return null;
        }
    }

    public static class Renderer extends DefaultTableCellRenderer
    {

        Color backgroundColor = getBackground();

        @Override
        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            Component c = super.getTableCellRendererComponent(
                    table, value, isSelected, hasFocus, row, column);
            this.setHorizontalAlignment(SwingConstants.CENTER);
            this.setVerticalAlignment(SwingConstants.CENTER);

            Model model = (Model) table.getModel();

            if (model.isWall(row, column))
            {
                c.setBackground(Color.black);
                c.setForeground(Color.white);
            }
            else if (model.isLighted(row, column))
            {
                c.setBackground(Color.yellow.brighter());
                c.setForeground(Color.black);
            }
            else
            {
                c.setBackground(Color.white);
                c.setForeground(Color.black);
            }
            return c;
        }
    }

    public static class Model extends ResizableTableModel
    {

        public Model(int rows, int cols)
        {
            super(rows, cols);
        }

        @Override
        public void setValueAt(Object aValue, int row, int col)
        {
            super.setValueAt(aValue, row, col); //To change body of generated methods, choose Tools | Templates.
            this.fireTableDataChanged();
        }

        public boolean isLighted(int row, int col)
        {
            String val;

            for (int i = row; i >= 0; i--)
            {
                val = (String) matrix[i][col];
                if (val == null)
                {
                    continue;
                }

                if (val.equals("*"))
                {
                    return true;
                }

                if (isWall(i, col))
                {
                    break;
                }
            }
            for (int i = row + 1; i < rows; i++)
            {
                val = (String) matrix[i][col];
                if (val == null)
                {
                    continue;
                }

                if (val.equals("*"))
                {
                    return true;
                }

                if (isWall(i, col))
                {
                    break;
                }
            }

            for (int i = col; i >= 0; i--)
            {
                val = (String) matrix[row][i];
                if (val == null)
                {
                    continue;
                }

                if (val.equals("*"))
                {
                    return true;
                }

                if (isWall(row, i))
                {
                    break;
                }
            }
            for (int i = col + 1; i < cols; i++)
            {
                val = (String) matrix[row][i];
                if (val == null)
                {
                    continue;
                }

                if (val.equals("*"))
                {
                    return true;
                }

                if (isWall(row, i))
                {
                    break;
                }
            }

            return false;
        }

        public boolean isWall(int row, int col)
        {
            return (matrix[row][col] != null && (matrix[row][col].equals("x") || StringUtilities.isNumeric((String) matrix[row][col])));
        }

    }
}
